﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using System.Configuration;

namespace WeatherFetcher
{
    class Program
    {
        static void Main(string[] args)
        {

            // Get the name of the folder
            string outputfolder = Properties.Settings.Default.OutputFolder;

            // checking if the directory exists. If it doesn't, create it
            DirectoryInfo df = new DirectoryInfo(outputfolder);

            if (!df.Exists)
            {
                df.Create();
            }

            // Creating a stringbuilder for the different XML-files
            
            StringBuilder sb = new StringBuilder();

            // Getting the URLs
            XmlDocument xDoc = new XmlDocument();

            // Load the XML-file
            xDoc.Load("urls.xml");

            // Get both input and output nodes
            XmlNodeList input = xDoc.GetElementsByTagName("link");
            XmlNodeList output = xDoc.GetElementsByTagName("file");
            // MessageBox.Show("Name: " + name[0].InnerText);
            int counter = input.Count;

            // Loop through the links and output-files
            for (int i = 0; i < counter; i++)
            {
                // used on each read operation
                byte[] buf = new byte[8192];

                // prepare the web page we will be asking for
                HttpWebRequest request = (HttpWebRequest)
                    WebRequest.Create(input[i].InnerText);

                // execute the request
                HttpWebResponse response = (HttpWebResponse)
                    request.GetResponse();

                // we will read data via the response stream
                Stream resStream = response.GetResponseStream();

                string tempString = null;
                int count = 0;

                do
                {
                    // fill the buffer with data
                    count = resStream.Read(buf, 0, buf.Length);

                    // make sure we read some data
                    if (count != 0)
                    {
                        // Translate from bytes to UTF8
                        tempString = Encoding.UTF8.GetString(buf, 0, count);
                        // translate from bytes to ASCII text
                        // tempString = Encoding.ASCII.GetString(buf, 0, count);

                        // continue building the string
                        sb.Append(tempString);
                    }
                }
                while (count > 0); // any more data to read?

                // Now we have some text, and so we shall save it
                // create a writer and open the file
                TextWriter tw = new StreamWriter(outputfolder+@"/" + output[i].InnerText);

                // write a line of text to the file
                tw.WriteLine(sb.ToString());

                // Clean sb-string
                sb.Length = 0;

                // close the stream
                tw.Close();
            }


        }
    } 
}
